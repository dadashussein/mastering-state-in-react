import { useState } from "react";
import { useDispatch } from "react-redux";
import { setSubscribed } from "../redux/features/checkSubscribe/subscriptionSlice";

export const useJoinAPI = (initialUrl, initialEmail) => {
  const [url, setUrl] = useState(initialUrl);
  const [email, setEmail] = useState(initialEmail);
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch(setSubscribed);

  const SUBSCRIBE = url + "/subscribe";
  const UNSUBSCRIBE = url + "/unsubscribe";

  const onSubscribe = async () => {
    setLoading(true);
    try {
      const response = await fetch(SUBSCRIBE, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ email }),
      });

      if (response.ok) {
        dispatch(setSubscribed(true));
        setEmail("");
      } else if (response.status === 422) {
        window.alert("Email is already in use");
        setEmail("");
      }
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  };

  const onUnsubscribe = async () => {
    setLoading(true);
    try {
      const response = await fetch(UNSUBSCRIBE, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ email }),
      });

      if (response.ok) {
        dispatch(setSubscribed(false));
        setEmail("");
      } else if (response.status === 422) {
        window.alert("Email is already in use");
      }
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  };

  return {
    email,
    setEmail,
    loading,
    setUrl,
    onSubscribe,
    onUnsubscribe,
  };
};

export default useJoinAPI;

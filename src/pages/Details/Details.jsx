import { useEffect, useState } from "react";

import { useNavigate, useParams } from "react-router-dom";
import "./details.css";

export const Details = () => {
  const { id } = useParams();
  const [community, setCommunity] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const { firstName, lastName, avatar, position } = community;
  const navigate = useNavigate();
  useEffect(() => {
    const fetchUsers = async () => {
      try {
        const response = await fetch(`http://localhost:3000/community/${id}`);
        if (!response.ok) {
          navigate("/not-found");
          return;
        }
        const data = await response.json();
        setCommunity(data);
        setIsLoading(false);
      } catch (error) {
        console.error(error);
      }
    };
    fetchUsers();
  }, [id, navigate]);
  return (
    <div>
      {isLoading ? (
        <h1>Loading...</h1>
      ) : (
        <div className="cart-content">
          <img src={avatar} alt="" />
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsum
            labore at molestias facere officia! Suscipit, fugiat? Reprehenderit
            dolorem exercitationem omnis!
          </p>
          <h1>
            {firstName} {lastName}
          </h1>
          <p>{position}</p>
        </div>
      )}
    </div>
  );
};

import Community from "../components/Community/Community";
import JoinProgram from "../components/JoinProgram/JoinProgram";

const MainPage = () => {
  return (
    <>
      <Community />
      <JoinProgram />
    </>
  );
};

export default MainPage;

import { Link } from "react-router-dom";
import "./notfound.css";

const NotFound = () => {
  return (
    <div className="notFound">
      <h1>Page Not found</h1>
      <p>
        Looks like you`ve followed a broken link or entered a URL that doesn`t
        exist on this site
      </p>
      <Link to="/"> ← Back to our site</Link>
    </div>
  );
};

export default NotFound;

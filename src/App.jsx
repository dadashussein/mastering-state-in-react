import { Route, Routes } from "react-router-dom";
import { Details } from "./pages/Details/Details.jsx";
import NotFound from "./pages/NotFound/NotFound.jsx";
import MainPage from "./pages/MainPage.jsx";
import Community from "./components/Community/Community.jsx";

const App = () => {
  return (
    <>
      <Routes>
        <Route path="/" element={<MainPage />} />
        <Route path="/details/:id" element={<Details />} />
        <Route path="/not-found" element={<NotFound />} />
        <Route path="/community" element={<Community />} />
      </Routes>
    </>
  );
};

export default App;

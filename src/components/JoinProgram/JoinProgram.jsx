import "./joinProgram.css";
import useJoinAPI from "../../hooks/useDataApi";
import { useSelector } from "react-redux";

const JoinProgram = () => {
  const { email, setEmail, loading, onSubscribe, onUnsubscribe } = useJoinAPI(
    "http://localhost:3000",
    ""
  );
  const { isSubscribed } = useSelector((state) => state.subscription);

  if (loading) return <h1>Loading...</h1>;

  return (
    <section className="join-section">
      <h1 className="join-title">Join Our Program</h1>
      <p className="join-subtitle">
        Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
      </p>
      <form
        onSubmit={!isSubscribed ? onSubscribe : onUnsubscribe}
        className="join-form"
      >
        <input
          className={isSubscribed ? "none" : ""}
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          type="email"
        />
        <button
          disabled={email === "" || loading}
          className={isSubscribed ? "none" : ""}
          type="submit"
        >
          Subscribe
        </button>
        <button disabled={loading} className={!isSubscribed ? "none" : ""}>
          unsubscribe
        </button>
      </form>
    </section>
  );
};

export default JoinProgram;

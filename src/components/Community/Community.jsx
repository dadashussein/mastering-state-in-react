import { Fragment, useEffect } from "react";
import "./community.css";
import { useDispatch, useSelector } from "react-redux";
import { getCommunity } from "../../redux/features/community/communitySlice";
import { toggleHide } from "../../redux/features/hideSection/hidingSlice";
import { Link } from "react-router-dom";

const Community = () => {
  const dispatch = useDispatch();
  const { community, isLoading } = useSelector((state) => state.community);
  useEffect(() => {
    dispatch(getCommunity());
  }, [dispatch]);

  const { isHidden } = useSelector((state) => state.hideSection);

  if (isLoading) return <h1>Loading...</h1>;

  return (
    <section className="community-section">
      <h1 className="content--title">
        Big Community of <br /> People Like You{" "}
      </h1>
      <button onClick={() => dispatch(toggleHide())} className="section-hide">
        {isHidden ? "Hide Section" : "Show Section"}
      </button>
      {isHidden && (
        <>
          <p className="content--subtitle">
            We’re proud of our products, <br /> and we’re really excited when we
            get feedback from our users.
          </p>

          <div className="content">
            {community.map((i) => (
              <Fragment key={i.id}>
                <div className="content--cards">
                  <img src={i.avatar} alt="" />
                  <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Ipsum labore at molestias facere officia! Suscipit, fugiat?
                    Reprehenderit dolorem exercitationem omnis!
                  </p>
                  <h1>
                    <Link to={`/details/${i.id}`}>
                      {i.firstName} {i.lastName}
                    </Link>
                  </h1>
                  <p>{i.position}</p>
                </div>
              </Fragment>
            ))}
          </div>
        </>
      )}
    </section>
  );
};

export default Community;

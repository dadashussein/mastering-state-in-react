import { createSlice } from "@reduxjs/toolkit";

export const subscriptionSlice = createSlice({
  name: "subscription",
  initialState: {
    isSubscribed: false,
  },

  reducers: {
    setSubscribed: (state, action) => {
      state.isSubscribed = action.payload;
    },
  },
});

export const { setSubscribed } = subscriptionSlice.actions;
export default subscriptionSlice.reducer;

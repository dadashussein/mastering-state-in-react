import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

const initialState = {
  community: [],
  isLoading: false,
};

export const getCommunity = createAsyncThunk("getCommunity", async () => {
  const data = await fetch("http://localhost:3000/community");
  const result = await data.json();
  return result;
});

export const communitySlice = createSlice({
  name: "community",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getCommunity.pending, (state) => {
      state.isLoading = true;
    });
    builder.addCase(getCommunity.fulfilled, (state, action) => {
      state.isLoading = false;
      state.community = action.payload;
    });
  },
});

export default communitySlice.reducer;

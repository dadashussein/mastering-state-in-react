import { createSlice } from "@reduxjs/toolkit";

export const hidingSlice = createSlice({
  name: "hideSection",
  initialState: {
    isHidden: true,
  },
  reducers: {
    toggleHide: (state) => {
      state.isHidden = !state.isHidden;
    },
  },
});

export const { toggleHide } = hidingSlice.actions;
export default hidingSlice.reducer;

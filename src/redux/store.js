import { configureStore } from "@reduxjs/toolkit";
import communityReducer from "./features/community/communitySlice";
import subscriptionReducer from "./features/checkSubscribe/subscriptionSlice";
import hidingReducer from "./features/hideSection/hidingSlice";

export const store = configureStore({
  reducer: {
    community: communityReducer,
    subscription: subscriptionReducer,
    hideSection: hidingReducer,
  },
});
